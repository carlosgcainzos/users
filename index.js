/* eslint-disable no-console */

const userController = require('./controllers/userController');

/**
 * users Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 * @returns {Object} null
 */
exports.users = (req, res) => {
  switch (req.method) {
    /** TODO: validate input user data
    *  Password must be a string
    */
    case 'GET':
      try {
        console.log('> users: handleIndex');
        userController.handleUsers(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    case 'POST':
      try {
        console.log('> users: createUser');
        userController.createUser(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    case 'PUT':
      try {
        console.log('> users: updateUser');
        userController.updateUser(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    case 'DELETE':
      try {
        userController.deleteUser(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    default:
      res.status(400).send({ message: 'Method not allowed' });
  }
};
