/* eslint-disable no-console */
const bcrypt = require('bcrypt');

module.exports = {
  toType(obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
  },
  /**
   * Hash data with bcrypto library.
   * @param {Object} data - Data to be hashed
   * @returns {String} Hashed data string with a 10 round salt
   */
  hash(data) {
    console.log('> utils.hash: ');
    return bcrypt.hashSync(data, 10);
  },
  /**
   * Check hashed passwords with bcrypto library.
   * @param {Object} sentPassword - Password which user sends.
   * @param {Object} userHashPassword - Password which is stored into DB.
   * @returns {Bool} Comparasion between passwords.
   */
  checkPassword(sentPassword, userHashPassword) {
    console.log('> utils.checkPassword');
    return bcrypt.compareSync(sentPassword, userHashPassword);
  },
  /**
   * Get Parms from request.
   * @param {Object} req - Express request.
   * @returns {Bool} True if params found.
   */
  params(req) {
    console.log('> utils.params');
    if (!req.params) {
      return false;
    }
    const path = req.params['0'];
    if (!path) {
      return false;
    }
    req.params.parts = path.split('/');
    while (req.params.parts[0] === '') {
      req.params.parts.shift();
    }
    if (req.params.parts.length === 0) {
      delete req.params.parts;
      return false;
    }
    return true;
  },
};
