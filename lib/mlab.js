/* eslint-disable no-console */

const axios = require('axios');
const utils = require('../lib/utils');


/**
 * Get User(s) from MLab".
 *
 * @param {Object} req Cloud Function request context.
 * @return {Promise} A list of users or error.
 */
async function indexUsers(req) {
  console.log('> mlab.indexUsers');

  const request = axios.get('https://api.mlab.com/api/1/databases/techu/collections/user',
    this.requestOptions(req));

  return request
    .then(result => result.data)
    .catch((error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log('< mlab.indexUsers: Response error: ',
          'status code: ', error.response.status,
          ', data: ', error.response.data,
          ', headers: ', error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log('< mlab.indexUsers: No response received: ', error.stack);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('< mlab.indexUsers: Error: ', error.message);
      }
      console.log('< mlab.indexUsers: Stack: ', error.stack);
      return Promise.reject(error);
    });
}

/**
 * Modify User(s) from MLab".
 *
 * @param {Object} req Cloud Function request context.
 * @param {String} deleteUser delete will be performed.
 * @return {Promise} A list of users or error.
 */
async function modifyUsers(req, deleteUser) {
  console.log('> mlab.modifyUsers');
  let putData = [];

  if (!deleteUser) {
    putData = { $set: req.body };
  }
  const request = axios.put('https://api.mlab.com/api/1/databases/techu/collections/user',
    putData,
    this.requestOptions(req));
  return request
    .then(result => result.data)
    .catch((error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log('< mlab.modifyUsers: Response error: ',
          'status code: ', error.response.status,
          ', data: ', error.response.data,
          ', headers: ', error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log('< mlab.modifyUsers: No response received: ', error.stack);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('< mlab.modifyUsers: Error: ', error.message);
      }
      console.log('< mlab.modifyUsers: Stack: ', error.stack);
      return Promise.reject(error);
    });
}

/**
 * Compose MLab query param.
 * @param {Object} id - User's id.
 * @returns {string} MLab query string.
 */
function getMLabQueryById(id) {
  let query;
  const type = utils.toType(id);

  switch (type) {
    case 'number':
      query = `{"id":${id}}`;
      break;
    default:
      query = `{"id":"${id}"}`;
  }
  return query;
}

/**
 * Compose MLab query param.
 * @param {Object} email - User's email.
 * @returns {string} MLab query string.
 */
function getMLabQueryByEmail(email) {
  return `{"email":"${email}"}`;
}

/**
 * Generate Axios Options.
 * @param {Object} req - Express request.
 * @returns {Object} Axios options.
 */
function requestOptions(req) {
  const dbKey = process.env.MLAB_API_KEY || '123';
  const options = {
    params: {},
    // Reject only if the status code is greater than or equal to 500
    validateStatus: status => status < 500,
  };

  if (utils.params(req)) {
    options.params.q = this.getMLabQueryById(req.params.parts[0]);
  } else if (req.body && 'email' in req.body) {
    options.params.q = this.getMLabQueryByEmail(req.body.email);
  }
  options.params.apiKey = dbKey;
  return options;
}

module.exports.indexUsers = indexUsers;
module.exports.modifyUsers = modifyUsers;
module.exports.requestOptions = requestOptions;
module.exports.getMLabQueryById = getMLabQueryById;
module.exports.getMLabQueryByEmail = getMLabQueryByEmail;
