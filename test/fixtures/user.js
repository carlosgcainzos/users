module.exports = [
  {
    _id: {
      $oid: '5bd09c63636d69ee5ca0045b',
    },
    id: 3,
    first_name: 'Dennet',
    last_name: 'Mackelworth',
    email: 'dmackelworth2@army.mil',
    gender: 'Male',
    accounts: [
      {
        iban: 'HR95 2254 1969 9076 9344 8',
        balance: -226262.82,
      },
      {
        iban: 'FR72 7824 8810 16RY PO3Z LV21 E46',
        balance: -951471.26,
      },
      {
        iban: 'FR11 8431 5187 18LW JGQV XOYW O22',
        balance: -597153.33,
      },
      {
        iban: 'KZ62 714C QZRP KH3U HDR1',
        balance: -567022.83,
      },
      {
        iban: 'SI66 5978 1197 5371 778',
        balance: 630962.02,
      },
    ],
  },
];
