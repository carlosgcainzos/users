// [START functions_http_integration_test]
const test = require('ava');
const Supertest = require('supertest');
const users = require('./fixtures/notInsertedUsers');

const supertest = Supertest('http://localhost:8010/techU/us-central1');
const mlab = Supertest('https://api.mlab.com/api/1/databases/techu/collections');

test.afterEach.always.cb('cleanup', (t) => {
  mlab
    .put('/user')
    .query({ apiKey: process.env.MLAB_API_KEY })
    .send([])
    .expect(200)
    .then(() => {
      t.end();
    });
});

test.serial.cb('users-get-index: should return list of Users', (t) => {
  mlab
    .post('/user')
    .query({ apiKey: process.env.MLAB_API_KEY })
    .send(users)
    .expect(200)
    .then((resMlab) => {
      t.is((resMlab.body.n), 2);
      supertest
        .get('/users')
        .expect(200)
        .then((resUsr) => {
          t.is((resUsr.body.length), 2);
          t.end();
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((error) => {
      console.log(error);
    });
});

test.skip('users-get-id: should return Users details', (t) => {
  /**
   * Functions simulator does not parse URL params.
   * This test should be made as system test
   */
  // supertest
  //   .get('/users/3')
  //   .expect(200)
  //   .expect((response) => {
  //     t.deepEqual(response.body, user);
  //   })
  //   .end(t.end);
});

test.serial.cb('users-post-user: should return 204', (t) => {
  supertest
    .post('/users')
    .send({
      id: 2000,
      first_name: 'trato',
      last_name: 'magic',
      email: 'trato@magic.com',
      password: '1234',
    })
    .expect(201)
    .then(() => {
      mlab
        .put('/user')
        .query({ q: { id: 2000 }, apiKey: process.env.MLAB_API_KEY })
        .send([])
        .expect(200)
        .then(() => { t.end(); });
    });
});

test.skip('users-delete-id: should return #Users deleted', (t) => {
  /**
   * Functions simulator does not parse URL params.
   * This test should be made as system test
   */
  // supertest
  //   .post('/users')
  //   .send({
  //     id: 101,
  //     first_name: 'bruce',
  //     last_name: 'wayne',
  //     email: 'bruce@wayne.com',
  //     password: '1234',
  //   })
  //   .expect(201);
  //
  // supertest
  //   .delete('/users/101')
  //   .expect(200)
  //   .end(t.end);
});

test.skip('users-put-id: should return #Users deleted', (t) => {
  /**
   * Functions simulator does not parse URL params.
   * This test should be made as system test
   */
  // supertest
  //   .post('/users')
  //   .send({
  //     id: 102,
  //     first_name: 'eric',
  //     last_name: 'wizard',
  //     email: 'eric@magician.com',
  //     password: '1234',
  //   })
  //   .expect(201);
  //
  // supertest
  //   .put('/users/102')
  //   .send({
  //     email: 'eric@magic.com',
  //   })
  //   .expect(200)
  //   .end(t.end);
});

test.serial.cb('users-delete-email: should return 204', (t) => {
  const user = {
    id: 1000,
    first_name: 'truco',
    last_name: 'dent',
    email: 'truco@dent.com',
    password: '1234',
  };

  mlab
    .post('/user')
    .query({ apiKey: process.env.MLAB_API_KEY })
    .send(user)
    .expect(200)
    .then((resMlab) => {
      t.is((resMlab.body.id), 1000);
      supertest
        .delete('/users')
        .send({ email: 'truco@dent.com' })
        .expect(204)
        .then(() => {
          t.end();
        });
    })
    .catch((error) => {
      console.log(error);
    });
});
// [END functions_http_integration_test]
