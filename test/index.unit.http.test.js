const test = require('ava');
const sandbox = require('sinon').createSandbox();

const gcpFunction = require('../index');
const userController = require('../controllers/userController');

test('users: GET should call handleUsers', (t) => {
  const handleUsersSpy = sandbox.spy(userController, 'handleUsers');

  const req = {
    method: 'GET',
    params: { 0: '' },
    body: {},
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  gcpFunction.users(req, res);
  t.true(handleUsersSpy.calledOnce);
});

test('users: OPTIONS should response with status 500', (t) => {
  const req = {
    method: 'OPTIONS',
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  gcpFunction.users(req, res);
  t.deepEqual(res.status.firstCall.args, [400]);
  t.deepEqual(res.send.firstCall.args, [{ message: 'Method not allowed' }]);
});

test('users: POST with user data should call createUser', (t) => {
  const createUserSpy = sandbox.spy(userController, 'createUser');
  const req = {
    method: 'POST',
    body: {
      first_name: 'truco',
      last_name: 'dent',
      email: 'truco@dent.com',
      password: '1234',
    },
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  gcpFunction.users(req, res);
  t.true(createUserSpy.calledOnce);
});

test('users: PUT with user data should call updateUser', (t) => {
  const updateUserSpy = sandbox.spy(userController, 'updateUser');
  const req = {
    method: 'PUT',
    body: {
      first_name: 'truco',
      last_name: 'dent',
      email: 'truco@dent.com',
      password: '1234',
    },
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  gcpFunction.users(req, res);
  t.true(updateUserSpy.calledOnce);
});

test('users: DELETE with user data should call deleteUser', (t) => {
  const deleteUserSpy = sandbox.spy(userController, 'deleteUser');
  const req = {
    method: 'DELETE',
    body: {
      id: 2,
    },
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  gcpFunction.users(req, res);
  t.true(deleteUserSpy.calledOnce);
});
